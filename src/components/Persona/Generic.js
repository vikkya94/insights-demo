import React, { Component } from 'react';
import '../../style.css';
import dollar from '../../imgs/dollar.svg';
import lady from '../../imgs/lady2.svg';
import map from '../../imgs/map.svg';
import api from '../../utils/api';

function SelectDay(props) {
  var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  return (
      <ul className="languages d-flex list-unstyled justify-content-between">
          {days.map(function (day) {
              return (
                
                  <li className="font-weight-bold cursor" 
                  style={day === props.selectesDay ? {color: '#d0021b'} : null}
                   onClick={props.onSelect.bind(null, day)}
                  
                      key={day}>
                      {day}
                  </li>
              )
          })}
      </ul>
  )
}

class Generic extends Component {
  constructor(props) {
    super(props)
    this.state = {
        selectedDay: 'Monday',
        repos: null,
    };
    this.aggregatedInfo = this.aggregatedInfo.bind(this);
    this.timeDesk = this.timeDesk.bind(this);
    this.timeMob = this.timeMob.bind(this);
  }
  componentDidMount(){
    this.aggregatedInfo(this.state.selectedDay);
  }
  aggregatedInfo(day){
    api.aggregatedInfo('both', day, '2017-11-01', '2017-11-30')
    .then(function(repos){
        this.setState(function(){
            return {selectedDay: day, repos: repos}
        })
    }.bind(this));
}
 timeDesk(){
  if(this.state.selectedDay === 'Tuesday'){
    return '11AM - 01PM';
  }else{
    return '10AM - 12PM';
  }
}
timeMob(){
  if(this.state.selectedDay === 'Tuesday'){
    return '06PM - 10PM';
  }else{
    return '08PM - 11PM';
  }
}
  render() {
    //console.log(this.state.repos)
    //devices data
    var data = !this.props.device ? [{ deviceName: 'Tyler', userCount: 0 }] : this.props.device;
    //console.log(data)
    var result = [];
    data.forEach(function (a) {
      if (!this[a.deviceName]) {
        this[a.deviceName] = { "deviceName": a.deviceName, "userCount": 0 };
        result.push(this[a.deviceName]);
      }
      this[a.deviceName].userCount += a.userCount;
    }, Object.create(null));
    var total = 0;
    for (var i = 0; i < result.length; i++) {  //loop through the array
        total += result[i].userCount;  //Do the math!
    }

    //product data
    let values = !this.props.topProduct ? { productName: 'Tyler', count: 123 } : this.props.topProduct; //array object list
    //  console.log(values)
    //  let maxValue = Math.max(...values.map(o => o.count));
    // let maxValue = values.reduce((a, b) => (a.count > b.count) ? a : b).count;
    let maxValue = 0;
    for (let i = 0; i < values.length; i++) {
      if (values[i].count > maxValue) {
        maxValue = values[i].count;
      }
    }
    
    return (
      <div className="container-fluid">
        <div className="row">
        <div className="col-md-12">
          <SelectDay  selectesDay = {this.state.selectedDay} onSelect={this.aggregatedInfo} />
          </div>
          <div className="col-md-5 pt-5">
            <p className=""><img src={dollar} alt="dollar" style={{ width: '20px', height: '20px' }} /> &nbsp; Her monthly purchases are above $50. (Contributes up to 70% of total revenue)</p>
            <p className=""><i className="fa fa-eye fa-1x" aria-hidden="true" ></i> &nbsp; She visits the <b>website {!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.avgVisits} </b> times a month.</p>
            <p className="text-capitalize "><i className="fa fa-shopping-bag" aria-hidden="true"></i> &nbsp;
                    {!this.props.topProduct ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.props.topProduct.filter(function (val) {
                      return val.count === maxValue

                    }).map(function (name) {
                      return name.productName
                    })} highest selling product with 87% of sales
                  </p>
          </div>
          <div className="col-md-3 text-center align-middle">
            <img src={lady} alt="person" /><br/>
            <b>Sidney</b><br />
            <b>Age 25-35</b><br />
            {/* <p><img src={map} alt="map" /> &nbsp; {!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.highValueCustomerCity},{!this.state.repos ? <i ></i> : this.state.repos.highValueCustomerCountry}</p> */}
            <p><img src={map} alt="map" /> &nbsp; California,United States </p>

          </div>
          <div className="col-md-4 pt-5">
            <p className=""><i className="fa fa-shopping-bag" aria-hidden="true"></i> &nbsp; Her bag value <b>amounts</b> to <b>${!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.avgBagValueDesktop}</b>.</p>
            <p className=""><i className="fa fa-bullseye" aria-hidden="true"></i>&nbsp; She <b>purchases</b> <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.avgItems} - {!this.state.repos ? <i></i> : this.state.repos.avgItems + 1}</b> items in an order when doing high value order.</p>
            <p className=""><i className="fa fa-bullseye" aria-hidden="true"></i>&nbsp; She <b>purchases</b> <b>2-3</b> items in an order when doing low value order.</p>
          </div>

        </div>
        <div className="row">

          <div className="col-md-6">
            <div className="card border-light " >
              <div className="card-header text-center"><i className="fa fa-desktop fa-2x align-middle" aria-hidden="true"></i> <b>Behaviour on Desktop (Contributes to {`${Math.round((result[0].userCount/total)*100)}%`})</b> </div>
              <div className="card-body">

                <div className="card-text">
                  <table className="table">
                    <tbody>
                      <tr style={{ borderTop: '0 !important' }}>
                        <td className="align-middle"><i className="fa fa-clock-o" aria-hidden="true"></i></td>
                        <td>She's most likely to buy between <b>{this.timeDesk()}</b>.</td>
                      </tr>
                      <tr>
                        <td className="align-middle"><i className="fa fa-bullhorn" aria-hidden="true"></i></td>
                        {/* <td><b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.dekstopPPC}</b> chance of buying from <b>paid</b> campaign.</td> */}
                        <td>Her chance of buying from <b>Google Paid search </b> campaign is <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.dekstopPPC}</b>.</td>
                      </tr>
                      <tr>
                        <td className="align-middle"><i className="fa fa-calendar-o" aria-hidden="true"></i></td>
                        <td>She has <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.weekOfDayBuyingChancesdesk}</b> chance of buying on a <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.dektopWeekOfDay}</b>.</td>
                      </tr>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="card border-light ">
              <div className="card-header text-center"><i className="fa fa-mobile fa-2x align-middle" aria-hidden="true"></i> <b>Behaviour on Mobile (Contributes to 39%)</b></div>
              <div className="card-body">
                <div className="card-text">
                  <table className="table">
                    <tbody>
                      <tr style={{ borderTop: '0 !important' }}>
                        <td className="align-middle"><i className="fa fa-clock-o" aria-hidden="true"></i></td>
                        {/* <td>She's most likely to buy between <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.mostBuyingHoursMobile} Hour</b>.</td> */}
                        <td>She's most likely to buy between <b>{this.timeMob()}</b>.</td>
                      </tr>
                      <tr>
                        <td className="align-middle"><i className="fa fa-bullhorn" aria-hidden="true"></i></td>
                        {/* <td><b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.mobilePPC}</b> chance of buying from <b>paid</b> campaign.</td> */}
                        <td>Her chance of buying from <b>Google Paid search </b> campaign is <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.mobilePPC}</b></td>
                      </tr>
                      <tr>
                        <td className="align-middle"><i className="fa fa-calendar-o" aria-hidden="true"></i></td>
                        <td>She has <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.weekOfDayBuyingChancesMob}</b> chance of buying on a <b>{!this.state.repos ? <i className="fa fa-spinner fa-pulse fa-1x fa-fw"></i> : this.state.repos.mobileWeekOfDay}</b>.</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    )
  }
}

export default Generic;