import React, { Component } from 'react';
import Generic from './Generic';
import New from './New';
import Returning from './Returning';
import api from '../../utils/api';

function SelectDay(props) {
  var days = ['Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday', 'Sunday'];
  return (
    <ul className="d-flex justify-content-between list-inline">
      {days.map(function (day) {
        return (
          <li className="list-inline-item" onClick={props.onSelect.bind(null, day)}
            key={day}>
            {day}
          </li>
        )
      })}
    </ul>

  )
}

class Persona extends Component {
  state = {
    aggregatedInfo: null,
    aggregatedInfoNew: null,
    aggregatedInfoRe: null,
    selectedDay: '',
    repos: null,
    daySelected: '',
    devices: null,
    topProduct: null,
    topProductNew: null,
    topProductRe: null,
  }

  handleDay = (dayValue) => {
    this.setState({ daySelected: dayValue })
  }
  // this.aggregatedInfo = this.aggregatedInfo.bind(this);
  //new user api data binding

  // this.aggregatedInfoNew = this.aggregatedInfoNew.bind(this);
  //new user api data binding

  // this.aggregatedInfoRe = this.aggregatedInfoRe.bind(this);

  //day selection
  // this.updateDay = this.updateDay.bind(this);

  // this.handleDay = this.handleDay.bind(this);
  // }
  // handleChange(e){
  //   this.setState({selectValue: e.target.value});
  //  }
  componentDidMount() {

    this.aggregatedInfo(this.state.selectedDay);
    //New user api data binding

    this.aggregatedInfoNew(this.state.selectedDay);
    //returning user api data binding

    this.aggregatedInfoRe(this.state.selectedDay);

    this.deviceLevel();
    this.topProduct();
    this.topProductNew();
    this.topProductRe();
  }


  aggregatedInfo(day) {

    api.aggregatedInfo('both', this.state.daySelected, '2017-11-01', '2017-11-30')
      .then(function (ai) {
        this.setState(function () {
          console.log(ai)
          // console.log(this.state.daySelected)
          return { aggregatedInfo: ai }
        })

      }.bind(this))
  }
  //New Users API calls

  aggregatedInfoNew(day) {
    api.aggregatedInfo('firstVisit', 'Monday', '2017-11-01', '2017-11-30')
      .then(function (ai) {
        this.setState(function () {
          return { aggregatedInfoNew: ai }
        })
      }.bind(this))
  }
  //Returing Users data binding

  aggregatedInfoRe(day) {
    api.aggregatedInfo('returning', 'Monday', '2017-11-01', '2017-11-30')
      .then(function (ai) {
        this.setState(function () {

          return { aggregatedInfoRe: ai }
        })
      }.bind(this))
  }

  deviceLevel() {
    api.deviceLevel('2017-11-01', '2017-11-30')
      .then(function (repo2) {
        this.setState(function () {
          return { devices: repo2 }
        })
      }.bind(this));
  }

  topProduct(){
    api.topProduct('both', '2017-11-01', '2017-11-30')
    .then(function(tp){
      this.setState(function(){
        return {topProduct: tp}
      })
    }.bind(this))
  }

  topProductNew(){
    api.topProduct('firstVisit', '2017-11-01', '2017-11-30')
    .then(function(tp){
      this.setState(function(){
        return {topProductNew: tp}
      })
    }.bind(this))
  }

  topProductRe(){
    api.topProduct('returning', '2017-11-01', '2017-11-30')
    .then(function(tp){
      this.setState(function(){
        return {topProductRe: tp}
      })
    }.bind(this))
  }
  //child to parent component

  updateDay(day) {
    api.aggregatedInfo('returning', day)
      .then(function (repos) {
        this.setState(function () {
          // console.log(repos)
          return { repos: repos }
        })
      }.bind(this));
  }
  render() {

    return (
      <div className="container">
        <div className="row">
          <h3 className="p-2">High Value Customer Persona <sub>(For the month of December)</sub></h3>
          {/* <div className="col-md-12">
          <SelectDay  onSelect={this.updateDay} />
          </div> */}
          <div className="col-12">
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li className="nav-item">
                <a className="nav-link active" id="generic-tab" data-toggle="pill" href="#generic" role="tab" aria-controls="generic" aria-selected="true">Overall</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" id="new-tab" data-toggle="pill" href="#new" role="tab" aria-controls="new" aria-selected="false">New</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" id="returning-tab" data-toggle="pill" href="#returning" role="tab" aria-controls="returning" aria-selected="false">Returning</a>
              </li>
            </ul>
            <div className="tab-content" id="pills-tabContent">
              <div className="tab-pane fade show active" id="generic" role="tabpanel" aria-labelledby="generic-tab">
                <Generic aggregatedInfo={this.state.aggregatedInfo} onSelectDay={this.handleDay} device={this.state.devices} 
                topProduct={this.state.topProduct}/>
              </div>
              <div className="tab-pane fade" id="new" role="tabpanel" aria-labelledby="new-tab">
                <New aggregatedInfo={this.state.aggregatedInfoNew} device={this.state.devices} 
                 topProduct={this.state.topProductNew}/>
              </div>
              <div className="tab-pane fade" id="returning" role="tabpanel" aria-labelledby="returning-tab">
                <Returning aggregatedInfo={this.state.aggregatedInfoRe} device={this.state.devices} 
                 topProduct={this.state.topProductRe} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Persona;