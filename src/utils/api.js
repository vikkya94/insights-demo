var axios = require('axios');
var qs = require('qs');
var config = {headers: {
        'content-type': 'application/x-www-form-urlencoded'}
}
module.exports = {
//   deviceLevel: function(){
//     var URI = window.encodeURI('http://192.168.0.78:8086/device-level-user-count?id=1418');
//     return axios.post(URI, qs.stringify({startDate: '2017-11-01', endDate: '2017-11-30'}),config)
//     .then(function(response){
//         return response.data
//     })
// }
deviceLevel: function(sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/device-level-user-count?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed}),config)
  .then(function(response){
      return response.data
  })
},
browserLevel: function(sd, ed){
  var encodedURI = window.encodeURI('http://52.207.62.35:8080/browser-level-user-count?id=1320');
  return axios.post(encodedURI, qs.stringify({startDate: sd, endDate: ed}),config) // Format yyyy-mm-dd
  .then(function(response){
      return response.data
  })
},
location: function(startDate, endDate){
  var URI = window.encodeURI('http://52.207.62.35:8080/location-based-user-count?id=1320');
  return axios.post(URI, qs.stringify({startDate: '2017-11-01', endDate: '2017-11-30'}), config)
  .then(function(response){
      return response.data;
  })
},
geo: function(){
  var URI = window.encodeURI('https://api.myjson.com/bins/178nst');
  return axios.get(URI)
  .then(function(res){
    return res.data;
  })
},
dayOfWeek: function(sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/day-of-week-user-count?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed}), config)
  .then(function(response){
      return response.data
  })
},
hourOfDay: function(sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/hour-of-day-user-count?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed}), config)
  .then(function(response){
      return response.data
  })
},
topProduct: function(usertype, sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/top-products?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed, userType: usertype}), config)
  .then(function(response){
      return response.data
  })
},
//channels api
channels: function(sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/channel-wise-user-count?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed}), config)
  .then(function(response){
    return response.data
  })
},

//Persona API data
aggregatedInfo: function(usertype, days, sd, ed){
  var URI = window.encodeURI('http://52.207.62.35:8080/aggregated-info?id=1320');
  return axios.post(URI, qs.stringify({startDate: sd, endDate: ed, type: 'bus', price: 50, userType: `${usertype}`, day: `'${days}'`}), config)
  .then(function(response){
    return response.data
  })
},
//dummy data 

time: function(){
  var URI = window.encodedURI('https://api.myjson.com/bins/ynm9d');
  return axios.get(URI)
  .then(function(res){
    return res.data;
  })
}
}
